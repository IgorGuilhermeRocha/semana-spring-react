package com.devsuperior.dsmeta.services.impl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.devsuperior.dsmeta.entities.Sale;
import com.devsuperior.dsmeta.repositories.SaleRepository;
import com.devsuperior.dsmeta.services.SmsService;
import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;

@Service
public class SmsServiceImpl implements SmsService {

    @Value("${twilio.sid}")
    private String twilioSid;

    @Value("${twilio.key}")
    private String twilioKey;

    @Value("${twilio.phone.from}")
    private String twilioPhoneFrom;

    @Value("${twilio.phone.to}")
    private String twilioPhoneTo;

    @Autowired
    private SaleRepository saleRepository;

    @Override
    public void sendSms(Long saleId) {

        Twilio.init(twilioSid, twilioKey);

        PhoneNumber to = new PhoneNumber(twilioPhoneTo);
        PhoneNumber from = new PhoneNumber(twilioPhoneFrom);

        Message message = Message.creator(to, from, this.getSale(saleId)).create();

        System.out.println(message.getSid());
    }

    private String getSale(Long saleId) {

        Optional<Sale> opSale = saleRepository.findById(saleId);

        String message = "error";

        if (!opSale.isEmpty()) {
            Sale sale = opSale.get();
            message = "O vendedor " + sale.getSellerName() + ", brilhou nas vendas com o total de : R$"
                    + String.format("%.2f", sale.getAmount());
        }

        return message;
    }
}
