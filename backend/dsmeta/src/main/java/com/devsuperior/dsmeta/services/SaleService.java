package com.devsuperior.dsmeta.services;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.devsuperior.dsmeta.entities.Sale;

public interface SaleService {
    public Page<Sale> findSales(String minDate, String maxDate, Pageable pageable);
}
