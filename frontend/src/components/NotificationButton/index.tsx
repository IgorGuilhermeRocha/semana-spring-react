import axios from 'axios';
import icon from '../../assets/imgs/notification-icon.svg';
import { BASE_URL } from '../../utils/request';
import './styles.css';

type Props = {
    saleId: number;
}

function handleClick(saleId: number) {
    axios(`${BASE_URL}/sales/${saleId}/notification`).then(response => {
        alert("Mensagem enviada com sucesso");
    })
}

function notificationButton({ saleId }: Props) {
    return (
        <>
            <div className="dsmeta-red-btn" onClick={() => { handleClick(saleId) }}>
                <img src={icon} alt="Notificar" />
            </div>

        </>

    )
}

export default notificationButton;
