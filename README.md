# Semana Spring React

Projeto feito na semana Spring React. 

## Sobre o projeto:
Projeto simples para análise de vendas por datas e envio de sms.

## Tecnologias utilizadas

- Back-end :

    Java, Java Spring, banco em memória h2, Twilio para o envio do sms.

- Front-end:

    Html, css, javascript, react, Typescript.

## Onde posso ver o projeto ?

Após Novembro de 2022, o plano gratuito do heroku será encerrado, contudo este llink estará indisponível e logicamente o front-end não funcionará corretamente.

O deploy do back-end foi feito no heroku: (https://dsmeta-igor-rocha.herokuapp.com/)

E o deploy do front no netlify: (https://dsmeta-igor-rocha.netlify.app/)

***Obs***: Em ambos: heroku, netlify pode ocorrer um atraso ao carregar a página, tendo em vista que eu uso contas gratuitas nestes sites. 


        


